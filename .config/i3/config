# set modifier
set $super Mod4
set $alt Mod1

# set font
font pango: Noto Sans 8

# Use Mouse+$super to drag floating windows to their wanted position
floating_modifier $super

#autostart
exec --no-startup-id hsetroot -center ~/.wallpaper.png
exec --no-startup-id xsettingsd &
exec --no-startup-id compton -b
exec --no-startup-id killall dunst;notify-send starting
exec --no-startup-id i3-msg 'workspace 1; exec i3-sensible-terminal'
exec --no-startup-id copyq
# start a terminal
bindsym $super+Return exec ~/.zqs_bin/toggle_i3_app "gnome-terminal" "Gnome-terminal" "gnome-terminal"
bindsym $super+Tab workspace back_and_forth
#bindsym $super+space workspace 1;focus

# start dmenu (a program launcher)
# bindsym $super+Shift+d exec i3-dmenu-desktop --dmenu="dmenu -i -fn 'Noto Sans:size=8'"
bindsym $super+q exec rofi -lines 12 -padding 18 -width 60 -location 0 -show drun -sidebar-mode -columns 3 -font 'Noto Sans 8'
# jump window quickly
bindsym $super+t exec rofi -lines 12 -padding 18 -width 60 -location 0 -show window -sidebar-mode -columns 3 -font 'Noto Sans 8'

# common apps keybinds
bindsym --release $alt+ctrl+q exec --no-startup-id flameshot gui

#bindsym $super+ exec flameshot gui
#bindsym Print exec scrot 'Cheese_%a-%d%b%y_%H.%M.png' -e 'viewnior ~/$f'
bindsym $super+0 exec i3lock -i ~/.wallpaper.png
bindsym $super+Shift+g exec ~/.zqs_bin/toggle_i3_app "chrome" "Google-chrome" "google-chrome"
bindsym $super+Shift+f exec nautilus;workspace 2;focus
bindsym $super+Shift+v exec ~/.zqs_bin/toggle_i3_app "code" "Code" "code"
bindsym $super+Shift+p exec ~/.zqs_bin/toggle_i3_app "chatgpt-desktop" "ChatGPT Desktop" "chatgpt-desktop"
bindsym $super+Shift+c exec --no-startup-id copyq toggle

#change volume
bindsym XF86AudioRaiseVolume exec amixer -q set Master 5%+
bindsym XF86AudioLowerVolume exec amixer -q set Master 5%-
bindsym XF86AudioMute exec amixer -D pulse set Master toggle

# music control
bindsym XF86AudioNext exec mpc next
bindsym XF86AudioPrev exec mpc prev
bindsym XF86AudioPlay exec mpc toggle
bindsym XF86AudioStop exec mpc stop

# kill focused window
bindsym $super+c kill
bindsym $alt+F4 kill

# change focus
bindsym $super+h focus left
bindsym $super+j focus down
bindsym $super+k focus up
bindsym $super+l focus right

# move focused window
bindsym $super+Shift+h move left
bindsym $super+Shift+j move down
bindsym $super+Shift+k move up
bindsym $super+Shift+l move right

# split in horizontal orientation
bindsym $super+bar split h

# split in vertical orientation
bindsym $super+minus split v

# enter fullscreen mode for the focused container
bindsym $super+f fullscreen toggle

# change container layout split
bindsym $super+s layout stacking
bindsym $super+w layout tabbed
bindsym $super+e layout toggle split

bindsym $super+a focus parent

# toggle tiling / floating
bindsym $super+space floating toggle

# change focus between tiling / floating windows
bindsym $super+Shift+space focus mode_toggle

# switch to workspace
bindsym $alt+Control+Right workspace next
bindsym $alt+Control+Left workspace prev
bindsym $super+1 workspace 1
bindsym $super+2 workspace 2
bindsym $super+3 workspace 3
bindsym $super+4 workspace 4
bindsym $super+5 workspace 5
bindsym $super+6 workspace 6

# move focused container to workspace
bindsym $super+Shift+1 move container to workspace 1
bindsym $super+Shift+2 move container to workspace 2
bindsym $super+Shift+3 move container to workspace 3
bindsym $super+Shift+4 move container to workspace 4
bindsym $super+Shift+5 move container to workspace 5
bindsym $super+Shift+6 move container to workspace 6

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $super+Shift+r restart

# exit i3
# bindsym $super+q exec "i3-nagbar -t warning -m 'Really, exit?' -b 'Yes' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt
        bindsym Return mode "default"
}
bindsym $super+r mode "resize"

# panel
bar {
        colors {
        background #2f343f
        statusline #2f343f
        separator #4b5262

        # colour of border, background, and text
        focused_workspace       #2f343f #bf616a #d8dee8
        active_workspace        #2f343f #2f343f #d8dee8
        inactive_workspace      #2f343f #2f343f #d8dee8
        urgent_workspacei       #2f343f #ebcb8b #2f343f
    }
        status_command i3status
}

# window rules, you can find the window class using xprop
# window rules, you can find the window class using xprop
for_window [class=".*"] border pixel 4
assign [class=Gnome-terminal] 1
assign [class=Nautilus|Google-chrome|Transmission-gtk] 2
assign [class="Code"] 3
assign [class=Geany|Evince|Gucharmap|Soffice|libreoffice*] 4
assign [class=Audacity|Vlc|mpv|Ghb|Xfburn|Gimp*|Inkscape] 5
assign [class="ChatGPT Desktop"] 5
assign [class=Lxappearance|System-config-printer.py|Lxtask|GParted|Pavucontrol|Exo-helper*|Lxrandr|Arandr] 6
for_window [class=Viewnior|feh|Audacious|File-roller|Lxappearance|Lxtask|Pavucontrol] floating enable
for_window [class=URxvt|Chrome|Code|Evince|Soffice|libreoffice*|mpv|Ghb|Xfburn|Gimp*|Inkscape|Vlc|Lxappearance|Audacity] focus
for_window [class=Xfburn|GParted|System-config-printer.py|Lxtask|Pavucontrol|Exo-helper*|Lxrandr|Arandr] focus
for_window [class="Code"] layout tabbed
for_window [class=copyq|zeal|Zeal] floating enable




# colour of border, background, text, indicator, and child_border
client.focused              #bf616a #2f343f #d8dee8 #bf616a #d8dee8
client.focused_inactive     #2f343f #2f343f #d8dee8 #2f343f #2f343f
client.unfocused            #2f343f #2f343f #d8dee8 #2f343f #2f343f
client.urgent               #2f343f #2f343f #d8dee8 #2f343f #2f343f
client.placeholder          #2f343f #2f343f #d8dee8 #2f343f #2f343f
client.background           #2f343f
